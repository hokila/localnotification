//
//  AppDelegate.m
//  testlocalnotification
//
//  Created by Hokila Jan on 13/1/22.
//  Copyright (c) 2013年 Hokila Jan. All rights reserved.
//

#import "AppDelegate.h"
const NSString* CODE = @"CODETOKEN";

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions{
    self.launchDate = [NSDate date];
    
    //app 原本沒開會跑到這邊
    UILocalNotification *localNotif = [launchOptions objectForKey:UIApplicationLaunchOptionsLocalNotificationKey];
    if (localNotif)
        [self showLocalNotificationInfo:localNotif];
    
    return YES;
}

- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)localNotif{
    //app 有開或在背景執行會跑在這邊
    [self showLocalNotificationInfo:localNotif];
}

-(void)showLocalNotificationInfo:(UILocalNotification *)localNotif{
//    NSLog(@"Recieved Notification %@",localNotif);
    NSDictionary* infoDic = localNotif.userInfo;
    NSLog(@"userInfo description=%@",[infoDic description]);
    NSLog(@"CODE = %@",[infoDic objectForKey:CODE]);
}

- (void)applicationWillResignActive:(UIApplication *)application{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application{
    UILocalNotification *notification=[[UILocalNotification alloc] init];
    if (notification!=nil) {
        NSLog(@">> support local notification launchDate = %@",self.launchDate);
        NSDate *now =[NSDate new];
        NSTimeInterval diff = [now timeIntervalSinceDate:self.launchDate];
        NSLog(@"diff = %f",diff);
        
        notification.fireDate=[now dateByAddingTimeInterval:60];
        notification.timeZone=[NSTimeZone defaultTimeZone];
//        根據開啟時間不同來決定要推什麼訊息
        if (diff >10.0f)
            notification.alertBody=@"第一種訊息！";
        else if (diff>5.0f)
            notification.alertBody=@"第二種訊息！";
        else
            notification.alertBody=@"第三種訊息！";
        
        notification.userInfo = [NSDictionary dictionaryWithObject:@"科科" forKey:CODE];;
        
        [[UIApplication sharedApplication]   scheduleLocalNotification:notification];
    }
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
