//
//  AppDelegate.h
//  testlocalnotification
//
//  Created by Hokila Jan on 13/1/22.
//  Copyright (c) 2013年 Hokila Jan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic,retain) NSDate *launchDate;

@end
